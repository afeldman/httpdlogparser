/*********************************************************************************************
* Copyright (c) 2011 Allan Feldman <allan.feldman(__AT__)gatech.edu>                         *
*                                                                                            *
* Permission is hereby granted, free of charge, to any person obtaining                      *
* a copy of this software and associated documentation files (the "Software"),               *
* to deal in the Software without restriction, including without limitation the              *
* rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell           *
* copies of the Software, and to permit persons to whom the Software is furnished            *
* to do so, subject to the following conditions:                                             *
*                                                                                            *
* The above copyright notice and this permission notice shall be included in all copies      *
* or substantial portions of the Software.                                                   *
*                                                                                            *
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,        *
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE  *
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR       *
* OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER     *
* DEALINGS IN THE SOFTWARE.                                                                  *
*********************************************************************************************/
/*
* 
* This code parses through a httpd access logfile
* and returns all of the transactions associated with a user
* in a separate text file
* 
* Written By: Allan Feldman
*/

/*////////////////////////////////////////////////////////////////////////////
// Includes                                                                 //
////////////////////////////////////////////////////////////////////////////*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*////////////////////////////////////////////////////////////////////////////
// Defines                                                                  //
////////////////////////////////////////////////////////////////////////////*/
#define NAME            "logParser"
#define VER             "1.1"
#define SZCH            (sizeof(char))
#define BUFSIZE         4096
#define BUF_STR         "4095"
#define EXIT(code) \
    {   if (outLog) free(outLog); if (head) freeList(head); \
        if(code) { printf("Press enter to continue..."); getc(stdin); } \
        return (code); }
#define BUILD_IP_EXIT(code) \
    {   if (fh_log) fclose(fh_log); \
        return (code); }
#define FILTER_LOG_EXIT(code) \
    {   if (fh_log) fclose(fh_log); if (fh_out) fclose(fh_out); \
        return (code); }
        
/*////////////////////////////////////////////////////////////////////////////
// typedefs                                                                 //
////////////////////////////////////////////////////////////////////////////*/
typedef union {
    unsigned int value;
    unsigned char byte[4];
} IP;
typedef struct node {
    unsigned int ip;    /* ip address in 4 bytes */
    struct node * Next; /* points to next thing in ip list */
} IP_LIST_NODE;

/*////////////////////////////////////////////////////////////////////////////
// Prototypes                                                               //
////////////////////////////////////////////////////////////////////////////*/
void freeList(IP_LIST_NODE * node);
IP_LIST_NODE * addNodeToFront(IP_LIST_NODE ** head, unsigned int ip);
int buildIpList(IP_LIST_NODE ** head, char * loginsFile, char * username);
int filterLog(IP_LIST_NODE * head, char * logFile, char * outFile);

/*////////////////////////////////////////////////////////////////////////////
// main                                                                     //
////////////////////////////////////////////////////////////////////////////*/
int main(int argc, char * argv[])
{
    /*  Setup Input String Pointers */
    char * inLog = NULL, * inLogins = NULL, * inUser = NULL;
    unsigned int len;
    
    /* Setup Output String Pointers */
    char * outLog = NULL;
    
    //setup ip list
    IP_LIST_NODE * head = NULL;
    
    if (argc!=4)
    {
        /* display syntax if no input file given */
        printf(NAME" v"VER" by Allan Feldman\n");
        printf("Syntax: "NAME" <log_filename> <logins_filename> <student_username>\n");
        EXIT(1);
    }
    
    inLog=argv[1];
    inLogins=argv[2];
    inUser=argv[3];
    /* build the ip list for that user */
    if (buildIpList(&head,inLogins,inUser))
    {   printf("Error: Unable to discover IP addresses\n"); EXIT(1);    }
    
    /*
        Make the output filename a function of the username
        output file: username_logfile.log
    */
    len = strlen(inUser);
    /* Allocate memory for _logfile.log and null terminator */
    if (!(outLog = malloc(SZCH*(len+13))))
    {   printf("Error: Out of memory\n");   EXIT(-1);   }
    
    /* copy over appropriate information from input user */
    strncpy(outLog,inUser,len); outLog[len]='\0';
    strcat(outLog,"_logfile.log");
    
    if (filterLog(head,inLog,outLog))
    {   printf("Error: Unable to filter log file\n"); EXIT(1);  }
    
    EXIT(0);
}

/*////////////////////////////////////////////////////////////////////////////
// freeList                                                                 //
// frees a list starting at IP_LIST_NODE * node                             //
////////////////////////////////////////////////////////////////////////////*/
void freeList(IP_LIST_NODE * node)
{
    if (node->Next!=NULL)
    {
        freeList(node->Next);
    }
    free(node);
}

/*////////////////////////////////////////////////////////////////////////////
// addNodeToFront                                                           //
// adds a node to the front of the ip list                                  //
////////////////////////////////////////////////////////////////////////////*/
IP_LIST_NODE * addNodeToFront(IP_LIST_NODE ** head, unsigned int ip)
{
    IP_LIST_NODE * newHead;
    /*  head is null pointer so list does not exist */
    if (!head)
        { return NULL; }
    
    /*  unable to allocate memory for list node */
    if (!(newHead=(IP_LIST_NODE *)malloc(sizeof(IP_LIST_NODE *))))
        { return NULL; }
    
    newHead->Next = (*head);    /*  set new head to point to previous head  */
    (*head)=newHead;            /* set new head pointer */
    newHead->ip = ip;           /*  set ip of the new head */
    return newHead;             /* return the pointer of the new head to indicate successful add */
}

/*////////////////////////////////////////////////////////////////////////////
// buildIpList                                                              //
// gets each user's associated IP's and puts them in the ip list            //
////////////////////////////////////////////////////////////////////////////*/
int buildIpList(IP_LIST_NODE ** head, char * loginsFile, char * username)
{
    FILE * fh_log;
    char loginUsername[BUFSIZE];
    unsigned int byte1,byte2,byte3,byte4;
    IP ip;
    
    if (!(fh_log=fopen(loginsFile,"r")))
    {   printf("Error: Unable to open logins file!\n"); BUILD_IP_EXIT(1); }
    
    while (fscanf(fh_log, "%d.%d.%d.%d,%"BUF_STR"s",&byte1, &byte2, &byte3, &byte4,loginUsername)==5)
    {
        /* if username matches */
        if (strcmp(username,loginUsername)==0)
        {
            ip.byte[3]=byte1;
            ip.byte[2]=byte2;
            ip.byte[1]=byte3;
            ip.byte[0]=byte4;
            /*  add ip node to the front of the ip list */
            if (addNodeToFront(head,ip.value)==NULL)
            {   printf("Error: Cannot add ip to list\n"); BUILD_IP_EXIT(1); }
        }
    }
    /*  if there was an error reading file or there were no IPs found   */
    if (!feof(fh_log) || (*head)==NULL)
    {   BUILD_IP_EXIT(1); }
    
    BUILD_IP_EXIT(0);
}

/*////////////////////////////////////////////////////////////////////////////
// filterLog                                                                //
// writes out to a file lines whose ip addresses match those in the ip list //
////////////////////////////////////////////////////////////////////////////*/
int filterLog(IP_LIST_NODE * head, char * logFile, char * outFile)
{
    FILE * fh_log, * fh_out;
    char buffer[BUFSIZE];
    unsigned int byte1,byte2,byte3,byte4;
    IP ip;
    
    IP_LIST_NODE * temp;
    
    if (!(fh_log=fopen(logFile,"r")))
    {   printf("Error: Unable to open log file!\n"); FILTER_LOG_EXIT(1); }
    
    if (!(fh_out=fopen(outFile,"w")))
    {   printf("Error: Unable to open file %s for writing!\n", outFile); FILTER_LOG_EXIT(1); }
    
    while (fscanf(fh_log, "%d.%d.%d.%d",&byte1, &byte2, &byte3, &byte4)==4)
    {
        fgets(buffer,BUFSIZE,fh_log);
        ip.byte[3]=byte1;
        ip.byte[2]=byte2;
        ip.byte[1]=byte3;
        ip.byte[0]=byte4;
        temp=head;
        while (temp!=NULL)
        {
            if (ip.value==temp->ip)
            {
                fprintf(fh_out,"%d.%d.%d.%d%s",byte1,byte2,byte3,byte4,buffer);
                break;
            }
            temp=temp->Next;
        }
    }
    
    if (!feof(fh_log))
    {   FILTER_LOG_EXIT(1); }
    
    FILTER_LOG_EXIT(0);
}
